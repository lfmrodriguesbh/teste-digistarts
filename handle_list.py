# -*- coding: utf-8 -*-


class HandleList(object):
    """
    Classe utilizada para implementar a lista de números referente aos inputs do usuário e seus métodos de controle, como:
    validadores de input, aplicação do método SET e ordenação.

    """

    def __init__(self):
        self.list_numbers = list()
        self.original_list = list()

    def __validate_type_int(self, _input):
        """
        Validada se um input feito pelo usuário contém somente números (todos os inputs são INT).
        Este validador é utilizado tanto para validar o número de itens na lista (N conforme descrição do problema), como um
        item da lista (K[i])
        :param _input: Input realizado pelo usuário, representando N ou K[i]
        :return: True se o input é formado somente por números, senão, False
        """

        try:
            int(_input)
            return True
        except Exception:
            return False

    def __validate_range_quantity_numbers(self, quantity_numbers):
        """
        Valida se o input referente a quantidade de itens na lista (N) está dentro do range especificado (1 <= N <= 1000)
        :param quantity_numbers: input N
        :return: True se N está dentro do range 1 <= N <= 1000, senão, False
        """
        return True if 1 <= quantity_numbers <= 1000 else False

    def __validate_range_item_list(self, item_list):
        """
        Valida se um item da lista (K[i]) está dentro do intervalo especificado (-1000 <= k[i] <= 1000)
        :param item_list: input k[i] feito pelo usuário
        :return: True se K[i] está dentro do intervalo especificado, senão, False
        """
        return True if -1000 <= item_list <= 1000 else False

    def validate_quantity_numbers(self, quantity_numbers):
        """
        Valida a quantidade de números N da lista, se é composto somente por números e se está dentro do range
        especificado (1 <= N <= 1000).
        :param quantity_numbers: input com a quantidade de números N
        :return: True se os dois validadores forem True, senão, False
        """

        if self.__validate_type_int(quantity_numbers):
            if self.__validate_range_quantity_numbers(int(quantity_numbers)):
                return True

        return False

    def validate_item_list(self, item_list):
        """
        Valida se o item da lista K[i] passa no teste dos dois validadores: se é composto somente por números e se está
        dentro do range especificado (-1000 <= N <= 1000)
        :param item_list: input K[i] com um item para adicionar à lista de números
        :return: True se os dois validadores forem True, senão False
        """

        if self.__validate_type_int(item_list):
            if self.__validate_range_item_list(int(item_list)):
                return True

        return False

    def add_item_list(self, item_list):
        """
        Adiciona um item K[i] já validado na lista de números. Uma lista com os números originais é mantida para fins de
        rastreabilidade.
        :param item_list: input com K[i] já validado
        :return: Nenhum retorno
        """

        # Verifica se o item da lista já foi convertido para inteiro
        if type(item_list) != int:
            raise Exception('O item da lista deve ser obrigatoriamente um número inteiro.')

        self.list_numbers.append(item_list)
        self.original_list.append(item_list)

    def __apply_set_list(self):
        """
        Aplica o builtin SET do Python na lista de número de forma a remover os números duplicados
        :return: Nenhum retorno
        """
        self.list_numbers = list(set(self.list_numbers))

    def __order_ascending_list_numbers(self):
        """
        Ordena a lista de números em ordem crescente
        :return: Nenhum retorno
        """
        self.list_numbers.sort()

    def print_list(self):
        """
        Imprime a lista de números processada, com o resultado do problema proposto
        :return: Nenhum retorno
        """

        print('**************************** LISTA PROCESSADA (RESULTADO) *******************************************')
        print('')

        for item in self.list_numbers:
            print(item, end=' ')

        print('')
        print('****************************************************************************************')

    def print_original_list(self):
        """
        Imprime a lista com os números originais.
        :return: Nenhum retorno
        """

        print('**************************** LISTA ORIGINAL *******************************************')
        print('')

        for item in self.original_list:
            print(item, end=' ')

        print('')
        print('****************************************************************************************')

    def exec(self):
        """
        Executa os métodos de remover itens duplicados da lista e ordenação
        :return: Nenhum retorno
        """
        self.__apply_set_list()
        self.__order_ascending_list_numbers()
