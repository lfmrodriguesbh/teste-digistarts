from handle_list import HandleList


def main():
    # Métodos que implementa regras da lista de números
    handle_list = HandleList()

    # Flag para indicar se o input referente à quantidade de números a ser inserido na lista (N), está validado
    # para dar continuidade ao fluxo do software
    valid_quantity_numbers = False

    # Quantidade de números (N) a ser inseridos na lista
    quantity_numbers = 0

    # Contador utilizado para controlar a quantidade de números já inseridos na lista
    valid_itens_list = 0

    # Coleta a quantidade de números (N) do usuário
    while not valid_quantity_numbers:
        quantity_numbers = input('Digite a Quantidade N de números a ser coletados: ')

        # Se quantity_numbers passar em todos os validadores, o mesmo é validado e é dada continuidade do fluxo
        # do software
        if handle_list.validate_quantity_numbers(quantity_numbers):
            valid_quantity_numbers = True
            quantity_numbers = int(quantity_numbers)

        else:
            print('Entrada incorreta, por favor, insira um número inteiro >= 1 e <= 1000')

    # Coleta os números K[i] para inserir na lista. A quantidade de números coletados é de acordo com quantity_numbers
    while valid_itens_list != quantity_numbers:
        item_list = input('Digite o {}º número da lista de entrada: '.format(valid_itens_list + 1))

        # Se o item K[i] passar nos testes dos seus validadores, é coletado o item K[i+1] (se existir). Senão
        # o mesmo K[i] é requisitado ao usuário
        if handle_list.validate_item_list(item_list):
            handle_list.add_item_list(int(item_list))
            valid_itens_list += 1

        else:
            print('Entrada incorreta, por favor, insira um número inteiro >= -1000 e <= 1000')

    # Executa os métodos SET e SORT na lista e mostra na tela
    handle_list.exec()
    handle_list.print_original_list()
    handle_list.print_list()


if __name__ == '__main__':
    main()